public class HotelModel
{
    public string? FloorNo { get; set; }
    public string? RoomName { get; set; }
    public int? KeyCard { get; set; }
    public bool? IsActive { get; set; } // true ว่าง , false ไม่ว่าง
    public string? FirstName { get; set; }
    public int? Age { get; set; }

}