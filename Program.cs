﻿

using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;

internal class Program
{
    public static void init(string output)
    {
        // TODO : Setting Log
        Log.Logger = new LoggerConfiguration()
            .Enrich.FromLogContext()
            .WriteTo.Console()
            .WriteTo.File(output, rollingInterval: RollingInterval.Infinite, restrictedToMinimumLevel: LogEventLevel.Information, outputTemplate: "{Message:lj}{NewLine}{Exception}")
            .CreateLogger();
    }


    private static async Task Main(string[] args)
    {
        try
        {
            Console.WriteLine("Enter your path input file:");
            string pathInput = Console.ReadLine();
            Console.WriteLine("Enter your path output file:");
            string output = Console.ReadLine();

            //Test
            if (string.IsNullOrWhiteSpace(pathInput))
            {
                pathInput = "InputFile/input.txt";
            }

            if (string.IsNullOrWhiteSpace(output))
            {
                output = "OutputFile/output.txt";
            }

            init(output);
            Log.Information("--------------------START--------------------------");

            // Open the text file using a stream reader.
            using StreamReader reader = new(pathInput);

            // Read the stream as a string.
            string text = await reader.ReadToEndAsync();
            var splitStr = text.Split("\n");
            var HotelList = new List<HotelModel>();

            // Create hotel
            foreach (var item in splitStr)
            {
                if (item.StartsWith("create_hotel"))
                {
                    var hotelSplit = item.Split(" ");
                    var result = "Hotel created with " + hotelSplit[1].ToString().Trim() + " floor(s), " + hotelSplit[2].ToString().Trim() + " room(s) per floor.";
                    Log.Information(result);

                    var floorNo = Int32.Parse(hotelSplit[1]);
                    var roomNo = Int32.Parse(hotelSplit[2]);
                    for (var floor = 1; floor <= floorNo; floor++)
                    {
                        for (var room = 1; room <= roomNo; room++)
                        {
                            var hotel = new HotelModel()
                            {
                                FloorNo = floor.ToString(),
                                RoomName = floor + room.ToString("D2"),
                                KeyCard = 0,
                                IsActive = true,
                                FirstName = "",
                                Age = null
                            };
                            HotelList.Add(hotel);
                        }
                    }
                }
            }

            if (HotelList.Count > 0)
            {
                foreach (var item in splitStr)
                {
                    // Chick In
                    if (item.StartsWith("book "))
                    {

                        var bookingRoom = item.Split(" ");
                        var roomName = bookingRoom[1].Trim();
                        var firstName = bookingRoom[2].Trim();
                        var age = Int32.Parse(bookingRoom[3].Trim());

                        var hotelNotRoom = HotelList.Where(x => x.RoomName == roomName).FirstOrDefault();
                        if (hotelNotRoom != null)
                        {
                            if (hotelNotRoom.IsActive == true)
                            {
                                hotelNotRoom.IsActive = false;
                                hotelNotRoom.FirstName = firstName;
                                hotelNotRoom.Age = age;
                                hotelNotRoom.KeyCard = GenerateKeyCard(HotelList);
                                Log.Information($"Room {roomName} is booked by {firstName} with keycard number {hotelNotRoom.KeyCard}.");
                            }
                            else
                            {
                                Log.Information($"Cannot book room {roomName} for {firstName}, The room is currently booked by {hotelNotRoom.FirstName}.");
                            }
                        }
                        else
                        {
                            Log.Information("ไม่มีห้องพักในโรงแรม");

                        }
                    }
                    // Chick Out
                    else if (item.StartsWith("checkout "))
                    {

                        var bookingRoom = item.Split(" ");
                        var keyCard = Int32.Parse(bookingRoom[1].Trim());
                        var firstName = bookingRoom[2].Trim();
                        var hotelCheckRoom = HotelList.Where(x => x.KeyCard == keyCard).FirstOrDefault();
                        if (hotelCheckRoom != null)
                        {
                            if (hotelCheckRoom.FirstName == firstName)
                            {
                                hotelCheckRoom.IsActive = true;
                                hotelCheckRoom.FirstName = null;
                                hotelCheckRoom.Age = null;
                                hotelCheckRoom.KeyCard = 0;
                                Log.Information($"Room {hotelCheckRoom.RoomName} is checkout.");
                            }
                            else
                            {
                                Log.Information($"Only {hotelCheckRoom.FirstName} can checkout with keycard number {hotelCheckRoom.KeyCard}.");
                            } 
                        }
                    }
                    
                    else if (item.StartsWith("list_available_rooms"))
                    {
                        var availableRooms = HotelList.Where(w => w.IsActive == true).Select(s => new { s.RoomName, s.FloorNo }).ToList();
                        availableRooms.ForEach(i => Log.Information($"{i.RoomName}"));
                    }

                    else if (item.ToString().Trim() == "list_guest")
                    {
                        var guest = HotelList.Where(w => w.IsActive == false).OrderBy(o => o.KeyCard).Select(s => s.FirstName).ToList();
                        Log.Information(string.Join(",", guest));
                    }

                    else if (item.StartsWith("get_guest_in_room "))
                    {
                        var bookingRoom = item.Split(" ");
                        var roomNo = bookingRoom[1].Trim();
                        var guestInRoom = HotelList.Where(w => w.RoomName == roomNo).Select(s => s.FirstName).FirstOrDefault();
                        Log.Information(guestInRoom);
                    }

                    else if (item.StartsWith("list_guest_by_age "))
                    {
                        var bookingRoom = item.Split(" ");
                        var symbol = bookingRoom[1].ToString();
                        var age = Int32.Parse(bookingRoom[2].Trim());
                        var guestInRoom = new List<HotelModel>();

                        if (symbol == ">")
                        {
                            guestInRoom = HotelList.Where(w => w.Age > age).ToList();
                        }
                        else if (symbol == "<")
                        {
                            guestInRoom = HotelList.Where(w => w.Age < age).ToList();
                        }
                        Log.Information(string.Join(",", guestInRoom.Select(s => s.FirstName)));
                    }

                    else if (item.StartsWith("list_guest_by_floor "))
                    {
                        var bookingRoom = item.Split(" ");
                        var floorNo = bookingRoom[1].ToString().Trim();
                        var guestByFloor = HotelList.Where(w => w.FloorNo == floorNo && w.IsActive == false).ToList();
                        Log.Information(string.Join(",", guestByFloor.Select(s => s.FirstName)));
                    }

                    else if (item.StartsWith("checkout_guest_by_floor "))
                    {
                        var bookingRoom = item.Split(" ");
                        var floorNo = bookingRoom[1].ToString().Trim();
                        var checkOutGetByFloor = HotelList.Where(w => w.FloorNo == floorNo && w.IsActive == false).ToList();
                        Log.Information("Room " + string.Join(",", checkOutGetByFloor.Select(s => s.RoomName)) + " are checkout.");
                        foreach (var room in checkOutGetByFloor)
                        {
                            room.KeyCard = 0;
                            room.IsActive = true;
                            room.FirstName = null;
                            room.Age = null;
                        }
                    }

                    else if (item.StartsWith("book_by_floor "))
                    {
                        var bookingRoom = item.Split(" ");
                        var floorNo = bookingRoom[1].ToString().Trim();
                        var firstName = bookingRoom[2].ToString();
                        var age = Int32.Parse(bookingRoom[3].ToString());
                        var bookByFloor = HotelList.Where(w => w.FloorNo == floorNo).ToList();
                        var roomCheckOut = bookByFloor.Find(f => f.IsActive == false);
                        if (roomCheckOut != null)
                        {
                            Log.Information($"Cannot book floor {floorNo} for {firstName}.");
                        }
                        else
                        {
                            foreach (var room in bookByFloor)
                            {
                                if (room.IsActive == true)
                                {
                                    room.KeyCard = GenerateKeyCard(HotelList);
                                    room.IsActive = false;
                                    room.FirstName = firstName;
                                    room.Age = age;
                                } 
                            }
                            Log.Information("Room " + string.Join(",", bookByFloor.Select(s => s.RoomName)) + " are booked with keycard number " + string.Join(",", bookByFloor.Select(s => s.KeyCard)));
                        }
                    }

                }
            }
            else
            {
                Log.Information("ไม่มีการสร้าง Hotel");
            }

            // TODO : PrintConsole
            // PrintConsole(HotelList, "ToTal");
            Log.Information("---------------------------------------------------");
            Console.ReadKey();

        }
        catch (IOException e)
        {
            Console.WriteLine("The file could not be read:");
            Console.WriteLine(e.Message);
            Console.ReadKey();
        }
    }

    public static int? GenerateKeyCard(List<HotelModel> HotelList)
    {
        var maxKeyCard = HotelList.Count;
        for (var keyNo = 1; keyNo <= maxKeyCard; keyNo++)
        {
            var keyCardExists = HotelList.Find(x => x.KeyCard == keyNo);
            if (keyCardExists == null)
            {
                return keyNo;
            }
        }
        return 0;
    }

    public static void PrintConsole(List<HotelModel> HotelList, string key)
    {
        Console.WriteLine($"--------- {key} --------------");
        foreach (var item in HotelList.OrderBy(x => x.FloorNo).ThenBy(x => x.RoomName))
        {
            Console.WriteLine($"FloorNo : {item.FloorNo} , RoomName : {item.RoomName} , KeyCard : {item.KeyCard} , IsActive : {item.IsActive} , FirstName : {item.FirstName} , Age : {item.Age}");
        }
    }


}